﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{

    private Rigidbody2D rigidBody2D;
    private SpriteRenderer BallRenderer;
    public GameManager gameManager;
    public PlayerController playerCont1;
    public PlayerController playerCont2;

    public float xInitialForce;
    public float yInitialForce;

    private Vector2 trajectoryOrigin;

    public Transform player1;
    public Transform player2;
    private int playerPowerUp; 
    private int playerPowerUp2;
    private bool powerup1timer = false;
    //public Vector3 player2;

    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        BallRenderer = GetComponent<SpriteRenderer>();
        RestartGame();

        trajectoryOrigin = transform.position;
    }

    void ResetBall()
    {
        transform.position = Vector2.zero;

        rigidBody2D.velocity = Vector2.zero;
    }

    void PushBall()
    {
        //float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);

        float yRandomInitialForce;
        int yInitialForceFix = Random.Range(0, 2);

        if (yInitialForceFix < 1)
        {
            yRandomInitialForce = -yInitialForce;
        }
        else
        {
            yRandomInitialForce = yInitialForce;
        }
        //==========================================================================
        float randomDirection = Random.Range(0, 2);

        if (randomDirection < 1.0f)
        {
            rigidBody2D.AddForce(new Vector2(-xInitialForce, yRandomInitialForce));
        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xInitialForce, yRandomInitialForce));
        }
    }

    void RestartGame()
    {
        ResetBall();
        playerPowerUp = 0;
        BallRenderer.color = Color.red;
        Invoke("PushBall", 2);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;

        if (collision.gameObject.name.Equals("Player1"))
        {
            playerPowerUp = 1;
        }else if (collision.gameObject.name.Equals("Player2"))
        {
            playerPowerUp = 2;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("PowerUp1"))
        {
            if (playerPowerUp == 1)
            {
                player1.transform.localScale = new Vector3(1, 3, 1);
                Destroy(collision.gameObject);
            }
            else if (playerPowerUp == 2)
            {
                player2.transform.localScale = new Vector3(1, 3, 1);
                Destroy(collision.gameObject);
            }

            StartCoroutine(IEpowerUp1());
        }

        if (collision.gameObject.name.Equals("PowerUp2"))
        {
            if (playerPowerUp == 1 || playerPowerUp == 2 )
            {
                BallRenderer.color = Color.white;
                playerPowerUp2 = 3;
                Destroy(collision.gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Player1") && playerPowerUp2 == 3)
        {
            playerCont2.IncrementScore();

            if (playerCont2.Score < gameManager.maxScore)
            {
                RestartGame();
            }
            playerPowerUp2 = 0;
        }

        if (collision.gameObject.name.Equals("Player2") && playerPowerUp2 == 3)
        {
            playerCont1.IncrementScore();

            if (playerCont1.Score < gameManager.maxScore)
            {
                RestartGame();
            }
            playerPowerUp2 = 0;
        }
    }

    IEnumerator IEpowerUp1()
    {
        if (playerPowerUp == 1)
        {
                yield return new WaitForSeconds(5);
                player1.transform.localScale = new Vector3(1, 1, 1);
        }else if (playerPowerUp == 2)
        {
                yield return new WaitForSeconds(5);
                player2.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }
}
